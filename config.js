
var Jasmine2HtmlReporter = require('protractor-jasmine2-html-reporter');

exports.config = {
    //directConnect: true,
  
    // Capabilities to be passed to the webdriver instance.
    capabilities: {
      'browserName': 'chrome'
    },
  
    // Framework to use. Jasmine is recommended.
    framework: 'jasmine',
  
    // Spec patterns are relative to the current working directory when
    // protractor is called.
    specs: ['specs/e2eCustomer.js'],
  
    onPrepare: function(){
        //browser.waitForAngularEnabled(false);

        //maximize window
        browser.driver.manage().window().maximize();

        //Jasmine html reporter
        jasmine.getEnv().addReporter(
          new Jasmine2HtmlReporter({
            savePath: 'target/screenshots'
          })
        );
    },

    suites: {
      Regression: ['specs/e2eCustomer.js', 'specs/e2eManager.js']
    }
  };