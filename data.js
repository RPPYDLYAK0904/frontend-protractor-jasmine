
module.exports = {
    Customer: {
        FirstCustomer: {
            firstName: "James",
            lastName: "Bond",
            postCode: "60630"
        },
        secondCustomer: {
            firstName: "Steven",
            lastName: "Kovi",
            postCode: "50095"
        },
        thirdCustomer: {
            firstName: "Leonardo",
            lastName: "Da Vinci",
            postCode: "59045"
        }
    }
}