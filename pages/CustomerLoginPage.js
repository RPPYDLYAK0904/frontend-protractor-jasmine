
function CustomerLogin(){

    this.login = element(by.css("button[class*='btn-default']"));
    this.label = element(by.css("div[class='form-group'] label"));

    this.selectName = function(name){
        element.all(by.repeater("cust in Customers")).each(function(item){
            item.getText().then(function(text){
                if(text == name)
                item.click();
            })
        })
    }
}

module.exports = new CustomerLogin();