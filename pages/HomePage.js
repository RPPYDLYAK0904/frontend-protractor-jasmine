
function Home(){
    this.customerLogin = element(by.css("button[ng-click*='customer']"));
    this.bankManagerLogin = element(by.css("button[ng-click*='manager']"));

    this.getURL = function(){
        browser.get("http://www.way2automation.com/angularjs-protractor/banking/#/login");
    };
};

module.exports = new Home();