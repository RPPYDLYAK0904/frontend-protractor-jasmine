
function OpenAccount(){
    
    this.selectName = function(name){
        element.all(by.repeater("cust in Customers")).each(function(item){
            item.getText().then(function(text){
                console.log(text);
                if(text == name)
                item.click();
            });
        });
    };

    this.selectCurrency = function(currency){
        element.all(by.css("select[id='currency'] option")).each(function(item){
            item.getText().then(function(text){
                if(text == currency)
                item.click();
            });
        });
    };

    this.process = element(by.css("button[type='submit']"));
};

module.exports = new OpenAccount();