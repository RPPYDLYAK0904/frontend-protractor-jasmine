

function Welcome(){

    this.name = element(by.css("[class*='fontBig']"));
    this.transactions = element(by.css("button[ng-click*='transactions']"));
    this.deposit = element(by.css("button[ng-click*='deposit']"));
    this.withdrawl = element(by.css("button[ng-click*='withdrawl']"));
    this.balance = element(by.css("div[class*='borderM']")).element(by.css("strong:nth-child(2)"));
    this.amountDeposit = element(by.css("form[ng-submit='deposit()'] input"));
    this.submitDeposit = element(by.css("form[ng-submit='deposit()'] button"));
    this.amountWthdrawl = element(by.css("form[ng-submit='withdrawl()'] input"));
    this.submitWithdrawl = element(by.css("form[ng-submit='withdrawl()'] button"))
    this.status = element(by.className("error ng-binding"));
}

module.exports = new Welcome();