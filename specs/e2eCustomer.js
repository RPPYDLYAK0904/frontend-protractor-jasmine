describe('Customer', function() {
    const home = require("../pages/HomePage.js");
    const customer = require("../pages/CustomerLoginPage.js");
    const welcome = require("../pages/WelcomePage.js");
    const transaction = require("../pages/TransactionPage.js");
 
    beforeAll(function(){
        home.getURL();
    });
 
    afterEach(function() {
         browser.sleep(3000);
     });
 
     afterAll(function() {
         browser.driver.manage().deleteAllCookies();
         browser.executeScript('window.localStorage.clear();');
     });
 
     it('should navigate to home page correctly', function() {
         console.log('Test: Customer- check URL');
            
        browser.getCurrentUrl().then(function(url){
            expect(url).toBe("http://www.way2automation.com/angularjs-protractor/banking/#/login");
        });
     });

     it('should take user to expected Customer Login page', function() {
        console.log('Test: Customer- check label name');

        home.customerLogin.click();

        customer.label.getText().then(function(text){
            expect(text).toContain("Your Name");
        });
    });

    it('should able to select the name from dropdown and login', function() {
        console.log('Test: Customer- select name and check welkoming');

        customer.selectName("Harry Potter");
        customer.login.click();
        welcome.name.getText().then(function(text){
            expect(text).toBe("Harry Potter");
        });
    });

    it('should able to make a deposit', function() {
        console.log('Test: Customer- make a deposit check');

       welcome.deposit.click();
       welcome.amountDeposit.sendKeys(1000);
       welcome.submitDeposit.click();

       welcome.status.getText().then(function(text){
           expect(text).toBe("Deposit Successful");
       });
    });

    it('should able to see transaction record after deposit', function() {
        console.log('Test: Customer- deposit transaction check');

        welcome.transactions.click();
        transaction.lastRecord.getText().then(function(text){
            expect(text).toBe("1000");
        });
        transaction.backButton.click();
    });

    it('should able to see balance update after deposit', function() {
        console.log('Test: Customer- balance after deposit check');

       let balanceBefore;
       welcome.balance.getText().then(function(text){
           balanceBefore = text;
       })
       welcome.deposit.click();
       welcome.amountDeposit.sendKeys(1000);
       welcome.submitDeposit.click();

       welcome.balance.getText().then(function(text){
           expect(Number(text)).toEqual(Number(balanceBefore) + 1000);
       });
    });

    it('should able to make a withdrawl', function() {
        console.log('Test: Customer- make a withdrawl check');

        welcome.withdrawl.click();
        welcome.amountWthdrawl.sendKeys(500);
        welcome.submitWithdrawl.click();

        welcome.status.getText().then(function(text){
            expect(text).toBe("Transaction successful");
        });
    });

    it('should able to see transaction record after withdrawl', function() {
        console.log('Test: Customer- withdrawl transaction check');

        welcome.transactions.click();
        transaction.lastRecord.getText().then(function(text){
            expect(text).toBe("500");
        })
        transaction.backButton.click();
    });

    it('should able to see balance update after withdrawl', function() {
        console.log('Test: Customer- balance after withdrawl check');

       let balanceBefore;
       welcome.balance.getText().then(function(text){
           balanceBefore = text;
       })
       welcome.withdrawl.click();
       welcome.amountWthdrawl.sendKeys(500);
       welcome.submitWithdrawl.click();

       welcome.balance.getText().then(function(text){
           expect(Number(text)).toEqual(Number(balanceBefore) - 500);
       });
    });

    it('should able to see Transaction Failed message when withdraw amount more than the balance', function() {
        console.log('Test: Customer- withdraw amount more than the balance check');

        welcome.withdrawl.click();
        welcome.amountWthdrawl.sendKeys(10000);
        welcome.submitWithdrawl.click();

        welcome.status.getText().then(function(text){
            expect(text).toContain("Transaction Failed");
        });
    });
 })