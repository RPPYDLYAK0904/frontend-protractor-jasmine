describe('Bank Manager', function() {
    const home = require("../pages/HomePage.js");
    const managerLogin = require("../pages/ManagerLoginPage.js");
    const addCustomer = require("../pages/AddCustomerPage.js");
    const openAccount = require("../pages/OpenAccountPage.js");
    const customers = require("../pages/CustomersPage.js");
    const data = require("../data");
    const using = require("jasmine-data-provider");
 
    beforeAll(function(){
        home.getURL();
    });
 
    afterEach(function() {
         browser.sleep(3000);
     });
 
     afterAll(function() {
         browser.driver.manage().deleteAllCookies();
         browser.executeScript('window.localStorage.clear();');
     });
 
     it('should navigate to home page correctly', function() {
         console.log('Test: Manager- check URL');
            
        browser.getCurrentUrl().then(function(url){
            expect(url).toBe("http://www.way2automation.com/angularjs-protractor/banking/#/login");
        });
     });

     it('should take user to expected Customer Login page', function() {
        console.log('Test: Manager- navigation');

        home.bankManagerLogin.click();

        browser.getCurrentUrl().then(function(url){
            expect(url).toContain("banking/#/manager");
        });
    });

    using(data.Customer, function(data, description){

        it('should be able to add new customer - ' + description, function() {
            console.log('Test: Manager- add customer check');
    
            managerLogin.addCustomer.click();
            addCustomer.firstName.sendKeys(data.firstName);
            addCustomer.lastName.sendKeys(data.lastName);
            addCustomer.postCode.sendKeys(data.postCode);
            addCustomer.addCustomerBtn.click();
            browser.switchTo().alert().getText().then(function(text){
                console.log(text);
                expect(text).toContain("successfully");
            })
            browser.switchTo().alert().accept();
        });
    });

    it('should be able to open account', function() {
        console.log('Test: Manager- open account check');

        managerLogin.openAccount.click();
        openAccount.selectName("Steven Kovi");
        openAccount.selectCurrency("Dollar");
        openAccount.process.click();
        browser.switchTo().alert().getText().then(function(text){
            console.log(text);
            expect(text).toContain("successfully");
        })
        browser.switchTo().alert().accept();
    });

    it('should be able to find customer in list', function() {
        console.log('Test: Manager- customer list check');

        managerLogin.customers.click();

        // customers.firstName.getText().then(function(fName){
        //     customers.lastName.getText().then(function(lName){
        //         expect(fName + " " + lName).toBe("Steven Kovi");
        //         console.log(fName + " " + lName);
        //     });
        // });
        let names = [];
        element.all(by.css("tr[ng-repeat*='Customers']")).all(by.css("td:nth-child(1)")).getText().then(function(text){
            names.push(text);
        });
        console.log(names);

            
        
    });
    
});